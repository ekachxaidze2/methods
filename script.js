// 1 filter by length
let Input = ["James", "Will", "Jack", "Nate", "Edward"];
let friends = Input.filter(friends => friends.length === 4);
console.log(friends); 


// 2 min and second min sum

let arr = [22, 8, 12, 19, 5]
function sumLow(nums) {
    let min = nums[0];
    let secondMin = nums[1];
    for (let i = 1; i < nums.length; i++) {
      if (nums[i] < min) {
        secondMin = min;
        min = nums[i];
      } else if (nums[i] < secondMin) {
        secondMin = nums[i];
      }
    }
    return min + secondMin;
  }
 console.log(sumLow(arr)) 


let minArr = [52, 76, 14, 12, 4]
function sumLows(nums) {
    let arr = nums.sort((a, b) => a - b);
    return arr[0] + arr[1];
}

console.log(sumLows(minArr))